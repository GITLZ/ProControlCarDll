#include "StdAfx.h"
#include "CConnectDevice.h"
#include "winuser.h"
#include "string"

CConnectDevice::CConnectDevice():
	_nDeviceType(4),
	_nDeviceInd(0),
	_reserved(0),
	_nCanIndex(0),
	_nSendFrameType(1),
	_nSendFrameFormat(0),
	_radioIDFormat(1),
	_deviceId(0)
{
}

CConnectDevice::~CConnectDevice()
{
}

CConnectDevice& CConnectDevice::funGetInstance()
{
	static CConnectDevice device;
	return device;
}

bool CConnectDevice::funOpenDevice(int deviceId)
{
	DWORD myReserved = 0;
	_deviceId = deviceId;

	_dwRel = VCI_OpenDevice(_nDeviceType, _nDeviceInd, myReserved);

	if (_dwRel == 1)
	{
		lzlog("%s \n", "open device successfully!");
	}
	else if (_dwRel == 0)
	{
		lzlog("%s \n", "failed to open device!");
		return false;
	}
	else
	{
		lzlog("%s \n", "USB-CAN does not exist or lost connection！");
		return false;
	}

	VCI_INIT_CONFIG myInitInfo[1];
	myInitInfo->Timing0 = 0x01;
	myInitInfo->Timing1 = 0x1C;
	myInitInfo->Filter = 0;
	myInitInfo->AccCode = 0x80000008;
	myInitInfo->AccMask = 0xFFFFFFFF;
	myInitInfo->Mode = 0;

	//初始化通道
	if (VCI_InitCAN(_nDeviceType, _nDeviceInd, _deviceId, myInitInfo) != 1)
	{
		lzlog("Init-CAN %d failed!", _deviceId);
		return false;
	}
	Sleep(100);

	//启动通道
	if (VCI_StartCAN(_nDeviceType, _nDeviceInd, _deviceId) != 1)
	{
		lzlog("Start-CAN %d failed!", _deviceId);
		return false;
	}

	return true;
}

bool CConnectDevice::funCloseDevice()
{
	_dwRel = VCI_CloseDevice(_nDeviceType, _nDeviceInd);

	if (_dwRel == 1)
	{
		lzlog("close device successfully!");
		return true;
	}
	else if (_dwRel == 0)
	{
		lzlog("failed to close device！");
		return false;
	}
	else
	{
		lzlog("USB-CAN does not exist or lost connection！");
		return false;
	}

	return true;
}

bool CConnectDevice::funResetDevice()
{
	DWORD myDwRel;
	myDwRel = VCI_ResetCAN(_nDeviceType, _nDeviceInd, _nCanIndex);
	if (myDwRel != 1)
	{
		return false;
	}
	return true;
}

CConnectDevice::CConDataArr CConnectDevice::funReceiveDeviceData()
{
	VCI_CAN_OBJ myPCanObj[2500];
	CConDataArr myData;

	//调用动态链接库接收函数
	int NumValue = VCI_Receive(_nDeviceType, _nDeviceInd, _deviceId, myPCanObj, 2500, 0);
	if (NumValue == -1) {

		lzlog("%s \n", "USB-CAN does not exist or lost connection！");
	}

	myData.len = NumValue;

	for (int num = 0; num < NumValue; ++num)
	{
		std::string str = "";
		for (int i = 0; i < (myPCanObj[num].DataLen); ++i)
		{
			char str1[100];
			sprintf_s(str1, "%02X", myPCanObj[num].Data[i]);
			str += str1;

		}
		myData.receive[num] = str;
		
		//lzlogNewLine("接收到的数据为：%s\n", receive[num].c_str());
	}

	return myData;
}

int CConnectDevice::funSendData(std::string strSendData)
{
	VCI_ClearBuffer(_nDeviceType, _nDeviceInd, _nCanIndex);

	VCI_CAN_OBJ mySendbuf[1];
	std::string myStr5[32];
	BYTE myBuf[50];
	BYTE mySendID[10];  //存储ID
	std::string myStrtemp, myStrtemp1;
	int myLen, myDatanum = 0, myIDnum = 0, myNewflag = 1, i;

	CString myStrSendID = "10 8F 10 10";  //ID, 每两位用空格隔开,两个为一个字节

	myLen = myStrSendID.GetLength();

	//将十六进制ID转换成十进制，每两位16进制存到SendID[]中
	for (i = 0; i < myLen; ++i)
	{
		myStrtemp = myStrSendID.GetAt(i);
		if (myStrtemp == " ")
		{
			myNewflag = 1;
		}
		else if (myNewflag == 1)
		{
			myNewflag = 0;
			myStrtemp = myStrSendID.GetAt(i);
			if (i == (myLen - 1))
			{
				myStr5[myIDnum] = "0" + myStrtemp;
			}
			else
			{
				myStrtemp1 = myStrSendID.GetAt(i + 1);

				if (myStrtemp1 == " ")
				{
					myStr5[myIDnum] = "0" + myStrtemp;
				}
				else
				{
					myStr5[myIDnum] = myStrtemp + myStrtemp1;
				}
			}
			mySendID[myIDnum] = funTwoHexToTen(myStr5[myIDnum]);
			myIDnum++;
			if (myIDnum >= 4)
			{
				break;
			}
		}//else if (newflag == 1)
	}//for (i = 0; i < len; ++i)

	//将十六进制data转换成十进制，每两位16进制存到buf[]中
	myNewflag = 1;
	myLen = strSendData.length();
	for (i = 0; i < myLen; ++i)
	{
		myStrtemp = strSendData.at(i);
		if (myStrtemp == " ")
		{
			myNewflag = 1;
		}
		else if (myNewflag == 1)
		{
			myNewflag = 0;
			myStrtemp = strSendData.at(i);
			if (i == (myLen - 1))
			{
				myStr5[myDatanum] = "0" + myStrtemp;
			}
			else
			{
				myStrtemp1 = strSendData.at(i + 1);
			
				if (myStrtemp1 == " ")
				{
					myStr5[myDatanum] = "0" + myStrtemp;

				}
				else
				{
					myStr5[myDatanum] = myStrtemp + myStrtemp1;
				}

			}
			myBuf[myDatanum] = funTwoHexToTen(myStr5[myDatanum]);
			myDatanum++;
			if (myDatanum >= 8)
			{
				break;
			}
		}//else if (myNewflag == 1)
	}//for (i = 0; i < myLen; ++i)

	mySendbuf->ExternFlag = _nSendFrameType;
	mySendbuf->DataLen = myDatanum;
	mySendbuf->RemoteFlag = _nSendFrameFormat;

	if (_nSendFrameFormat == 1)//if remote frame, data area is invalid
	{
		for (i = 0; i < myDatanum; ++i)
		{
			myBuf[i] = 0;
		}
	}

	if ((mySendbuf->ExternFlag) == 1)//
	{
		mySendbuf->ID = ((mySendID[0] << 24) | (mySendID[1] << 16) | (mySendID[2] << 8) | mySendID[3]) & 0x1FFFFFFF;
	}
	else//basic frame ID
	{
		mySendbuf->ID = ((mySendID[2] << 8) | mySendID[3]) & 0x7FF; //ID对齐方式，默认右对齐。左对齐功能关闭，强制右对齐。
	}

	for (i = 0; i < myDatanum; ++i)
	{
		mySendbuf->Data[i] = myBuf[i];
	}

	mySendbuf->SendType = 1;
	mySendbuf->TimeFlag = 0;
	mySendbuf->TimeStamp = 0;

	/******************************获取发送信息完毕***********************/
	
	int flag;
	if ((_nCanIndex == 1) && (_nDeviceType != VCI_USBCAN2))
	{
		lzlog("%s \n", "the device only support CAN index 0");
		_nCanIndex = 0;
	}

	//调用动态链接库发送函数
	flag = VCI_Transmit(_nDeviceType, _nDeviceInd, _nCanIndex, mySendbuf, 1);//CAN message send
	if (flag < 1)
	{
		if (flag == -1) {
			lzlog("%s \n", "failed- device not open");
			return -1;
		}
		else if (flag == 0) {
			lzlog("%s \n", "send error");
			return 0;
		}

	}
	else {
		std::string str = "";
		byte data;
		for (i = 0; i < mySendbuf->DataLen; i++)
		{
			data = mySendbuf->Data[i];

			char str1[100];
			sprintf_s(str1, "%02X", data);
			str += str1;
			str += " ";
		}
	}

	return 1;
}

int CConnectDevice::funTwoHexToTen(std::string str)
{
	int myLen = str.length();
	if (myLen == 2)
	{
		int myNuma = funOneHexToTen(str[0]);
		int myNumb = funOneHexToTen(str[1]);
		if (myNuma == 16 || myNumb == 16)
		{
			lzlog("%s \n", "Format error!");
			return 256;
		}
		else
		{
			return myNuma * 16 + myNumb;
		}
	}
	else
	{
		lzlog("%s \n", "input length must be 2!");
		return 256;
	}
}

int CConnectDevice::funOneHexToTen(char c)
{
	if ((c >= '0') && (c <= '9'))
	{
		return c - 0x30;
	}
	else if ((c >= 'A') && (c <= 'F'))
	{
		return c - 'A' + 10;
	}
	else if ((c >= 'a') && (c <= 'f'))
	{
		return c - 'a' + 10;
	}
	else
	{
		return 0x10;
	}
}
