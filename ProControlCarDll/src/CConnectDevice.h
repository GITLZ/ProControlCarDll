#pragma once
#include "ControlCAN.h"
#include "string"

#pragma region lzlog...

#define MAX_PATH 260
//调试窗口输出+换行
#define lzlogNewLine( fmt, ... ) {lzlog( fmt, ##__VA_ARGS__  );OutputDebugStringA("\r\n");}

//调试窗口输出
#define lzlog( fmt, ... ) {char szBuf[MAX_PATH]; sprintf_s(szBuf,fmt, ##__VA_ARGS__ ); OutputDebugStringA(szBuf);}

#pragma endregion

/// @brief 用于连接设备的类
class __declspec(dllexport) CConnectDevice
{
public:
	/// @brief 接收数据的结构
	struct CConDataArr
	{
		std::string receive[2500]; ///< string字符串类型
		int len;                   ///< 长度
	};

	CConnectDevice();
	~CConnectDevice();

	/// @brief 获取设备单例
	/// @return 设备单例
	static CConnectDevice& funGetInstance();

	/// @brief 用于打开设备的函数
	/// @param deviceId 设备编号，默认为0
	/// @return 返回true为打开正确，false为打开失败
	bool funOpenDevice(int deviceId = 0); 

	/// @brief 用于关闭设备的函数
	/// @return 返回true为打开正确，false为打开失败
	bool funCloseDevice();

	/// @brief 用于重置设备的函数
	/// @return 返回true为打开正确，false为打开失败
	bool funResetDevice();

	/// @brief 用于接收设备数据的函数
	/// @return 接收到的数据
	CConDataArr funReceiveDeviceData();

	/// @brief 用于发送数据的函数
	/// @param strSendData 所要发送的数据，string类型
	/// @return 返回1表示正常，0表示发送失败，-1表示设备没有打开
	int funSendData(std::string strSendData);

private:
	/// @brief 两位字符串16进制数字转10进制数
	/// @param str 所要转换的字符串
	/// @return 返回十进制数
	int funTwoHexToTen(std::string str);

	/// @brief 一位字符串16进制数字转10进制数
	/// @param c 所要转换的字符
	/// @return 返回十进制数
	int funOneHexToTen(char c);

private:
	int _nDeviceType;       ///< 设备类型,USBCAN-2A或USBCAN-2C或CANalyst-II 
	int _nDeviceInd;        ///< 设备索引,第1个设备 
	int _reserved;          ///< 保留参数，通常为 0。
	int _nCanIndex;         ///< CAN通道索引。第几路 CAN。即对应卡的CAN通道号，CAN1为0，CAN2为1。第1个通道 
	int _nSendFrameType;    ///< Frame 1 代表Extend扩展帧 0代表标准帧
	int _nSendFrameFormat;  ///< 发送帧格式
	int	_radioIDFormat;     ///< 无线电ID格式
	int _deviceId;          ///< 设备编号
	DWORD _dwRel;           ///< 打开设备返回参数
};